import React, { useEffect, useState } from "react";
import { movieService } from "../../../services/movieService";
import { Tabs } from "antd";
import MovieTabItem from "./MovieTabItem";
const onChange = (key) => {
  console.log(key);
};

export default function MovieTabs() {
  const [dataMovie, setDataMovie] = useState([]);
  useEffect(() => {
    movieService
      .getPhimTheoHeThongRap()
      .then((res) => {
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderHeThongRap = () => {
    return dataMovie.map((heThongRap) => {
      return {
        label: (
          <img
            className="w-16 h-16 object-cover"
            src={heThongRap.logo}
            alt="true"
          />
        ),
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            className="movie-tab-2"
            style={{ overflowY: "scroll" }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                label: <p>{cumRap.tenCumRap}</p>,
                key: cumRap.maCumRap,
                children: cumRap.danhSachPhim.map((phim) => {
                  return <MovieTabItem movie={phim} />;
                }),
              };
            })}
          />
        ),
      };
    });
  };

  return (
    <div>
      <Tabs
        className="my-32 movie-tab"
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderHeThongRap()}
      />
    </div>
  );
}
