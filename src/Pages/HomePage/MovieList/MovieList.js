import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";

export default function MovieList({ movieArr }) {
  let renderMovieList = () => {
    return movieArr.map((item, index) => {
      return (
        <Card
          key={index}
          hoverable
          style={{
            width: "100%",
          }}
          cover={
            <img
              alt="example"
              src={item.hinhAnh}
              className="h-80 object-cover"
            />
          }
        >
          <NavLink
            className="bg-red-500 px-5 py-2 rounded text-white text-center"
            to={`/detail/${item.maPhim}`}
          >
            Detail
          </NavLink>
        </Card>
      );
    });
  };
  return <div className="grid grid-cols-5 gap-5">{renderMovieList()}</div>;
}
